/*
 * Sample program for DokodeModem
 * trasnsmit and receive
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>
#include <SlrModem.h>

// 送信周期： LoRaだと通信速度が遅いので通信台数とデータ長しだいですが3秒は間隔をあけた方がいいです。
#define SEND_PERIOD_MS 10000

const uint8_t CHANNEL = 0x10;   // 10進で16チャネルです。通信相手と異なると通信できません。
const uint8_t DEVICE_DI = 0x00; // 通信相手のIDです。0は全てに届きます。
const uint8_t DEVICE_EI = 02;   // 自分のIDです
const uint8_t DEVICE_GI = 0x02; // グループIDです。通信相手と異なると通信できません。

DOKODEMO Dm = DOKODEMO();
SlrModem modem;

int count = 0;
uint32_t _elapsetime;

void setup()
{
  Dm.begin(); // 初期化が必要です。

  // デバッグ用　USB-Uart
  SerialDebug.begin(115200);

  // モデム uart
  UartModem.begin(MLR_BAUDRATE);

  // モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  // モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  // 各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存するようにしてください。
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false); // 送信毎に通信相手を変更したい場合は、送信毎に変更してください
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);

  _elapsetime = millis(); // ここからSEND_PERIOD_MS周期で送信されます
}

void loop()
{
  uint8_t recvdata[255];
  char data[10];

  modem.Work(); // ループ処理。受信データを処理しています。

  if (modem.HasPacket())
  {
    const uint8_t *pData;
    uint8_t len{0};

    SerialDebug.println("\r\n*** received ***");

    // 受信データのポインタとサイズを取得
    modem.GetPacket(&pData, &len);

    // 受信データ取得
    memcpy(&recvdata[0], pData, len);

    // 受信データ開放
    modem.DeletePacket();

    // 受信データ出力。受信データに応じて必要な処理に変更してください。
    // 受信データのチェックは必要です。下記のようなのは誤動作の元です。
    SerialDebug.write(recvdata, len);
    SerialDebug.println();

    // 受信環境のチェックは動作確認に必要になることが多いです。
    // 受信した時のRSSI値を読みます。
    int16_t rssi{0};
    modem.GetRssiLastRx(&rssi);
    SerialDebug.println("Rx RSSI:" + String(rssi) + "dBm");

    // 環境RSSI値を読みます
    int16_t env_rssi{0};
    modem.GetRssiCurrentChannel(&env_rssi);
    SerialDebug.println("Env RSSI:" + String(env_rssi) + "dBm");
  }

  uint32_t tmpTime = millis();
  if ((tmpTime - _elapsetime) >= SEND_PERIOD_MS)
  {
    _elapsetime = tmpTime;

    // 送信データ作成。下記の場合8バイトのASCIIですが、Binaryでも構いません。
    // 他の通信データが届いても誤動作しない仕組みが必要です。下記のような単純なパケットでは誤動作の元です。
    int size = sprintf(data, "REQ:%04d", count++);

    //@DTコマンドを実行し、送信結果を戻します。@DT08と\r\nは自動で付加されます。
    auto rc = modem.TransmitData((uint8_t *)data, size);
    if (rc == SlrModemError::Ok)
    {
      // 送信完了
      SerialDebug.println("\r\n*** Send Req Ok ***");
    }
    else
    {
      // キャリアセンスによって送信できなかったことを示します。
      // 必要に応じて時間をおいてからリトライ送信してください。
      SerialDebug.println("\r\n### Send Ng... ###");
    }
  }
}
